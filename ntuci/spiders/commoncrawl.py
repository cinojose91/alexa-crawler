# -*- coding: utf-8 -*-
import scrapy
from ntuci.items import  Website

class CommoncrawlSpider(scrapy.Spider):
    name = "commoncrawl"
    #allowed_domains = ["www.abc.com"]
    #start_urls = (
    #    'http://www.www.abc.com/',
    #)

    def parse(self, response):
      #  sites = response.css('#site-list-content > div.site-item > div.title-and-desc')
      #  items = []

      #  for site in sites:
      #      item = Website()
      #      item['name'] = site.css(
      #          'a > div.site-title::text').extract_first().strip()
      #      item['url'] = site.xpath(
      #          'a/@href').extract_first().strip()
      #      item['description'] = site.css(
      #          'div.site-descr::text').extract_first().strip()
      #      items.append(item)
       item = Website()
       item['name']=response.css('title::text').extract()
       item['description']= response.xpath('//body//p//text()').extract()
       return item
